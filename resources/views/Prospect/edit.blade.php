@extends('loyout.principal')
@section('title', 'prospect')
@section('content')
<div class="container pt-3">
    <h3 class="text-center">Editar Informacion</h3>
     <div class="form row">
       <form action="{{route('prospect.update',$prospects)}}" method="post" >
        @csrf
        @method('put');
           <div class="form-group">
               <label for="name">Nombre</label>
               <input type="text" class="form-control" name="name" value="{{$prospects->name}}"required>
           </div>
           <div class="form-group mt-4">
            <label for="name">Correo Electronico</label>
            <input type="email" class="form-control" name="email" value="{{$prospects->email}}"required>
        </div>
        <div class="form-group mt-4">
            <label for="name">Tipo de servicio</label>
            <select name="TypeServices" id="" value="{{$prospects->tipeServices}}">
                <option value="App-Movil">App-Movil</option>
                <option value="Sistema-Web">Sistema-Web</option>
                <option value="Sistema-escritorio">App-escritorio</option>
            </select>
        </div>
        <div class="form-group mt-4">
            <label for="Description">Descripcion</label>
             <textarea name="Description" cols="10" rows="10" class="form-control">{{$prospects->Description}}</textarea>
        </div>
        <div class="form-group mt-4">
            <input type="submit" class="btn btn-success" value="Actualizar prospecto">
        </div>
       </form>
     </div>

 </div>
 
@endsection
