<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\prospect;
use Illuminate\Support\Carbon;

class prospectseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $prospect = new prospect();
        $prospect->name = "Lazaro Rodriguez";
        $prospect->email ="Lazaro_1986@hotmail.com";
        Carbon::today($prospect->email_verified_at)->shiftTimezone('America/Hermosillo')->format('d/m G/h e');
        $prospect->typeServices= "sistema escritorio";
        $prospect->Description = "Desarrollo de un sistema escritorio para el acceso escolar";
        Carbon::today($prospect->update_at)->shiftTimezone(' America/Hermosillo')->format('d/m G/h e');
        Carbon::today($prospect->create_at)->shiftTimezone('America/Hermosillo')->format('d/m G/h e');
        $prospect->save();
        $prospect2 = new prospect();
        $prospect2->name = "Juan Rodriguez";
        $prospect2->email ="juan_1986@hotmail.com";
        Carbon::today($prospect2->email_verified_at)->shiftTimezone('America/Hermosillo')->format('d/m G/h e');
        $prospect2->typeServices= "App movil";
        $prospect2->Description = "Desarrollo de un sistema escritorio para el acceso escolar";
        Carbon::today($prospect2->update_at)->shiftTimezone(' America/Hermosillo')->format('d/m G/h e');
        Carbon::today($prospect2->create_at)->shiftTimezone('America/Hermosillo')->format('d/m G/h e');
        $prospect2->save();
        
    }
}
