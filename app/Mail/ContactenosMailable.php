<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactenosMailable extends Mailable
{
    use Queueable, SerializesModels;
     public $subject="informacion de prospecto";
     public $contacto;
    /**
     *
     * @return void
     */
    public function __construct($Contacto)
    {
        $this->contacto=$Contacto;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail/email');
    }
}
