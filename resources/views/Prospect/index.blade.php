@extends('loyout.principal')
@section('title', 'prospect')
@section('content')
<div class="container pt-3">
    <h3 class="text-center">Lista de posibles clientes</h3>
    <div class="row pt-2">
        <div class="col">
            <a class="btn btn-outline-info btn-sm" href="{{route('prospect.create')}}"><i class="fas fa-plus"></i></a>
        </div>
    </div>
    <table class="table table-stripper table-sm mt-2">
        <thead class="bg-dark text-white">
            <div class="row bg-dark text-white mt-2">
                        <nav class="nav">
                            <div class="col-2">
                                <a class="nav-link text-white">Nombre</a>
                            </div>
                              <div class="col-3">
                            <a class="nav-link text-white">Correo</a>
                              </div>
                              <div class="col-2">
                                <a class="nav-link text-white">Servicio</a>
                              </div>
                            <div class="col-5">
                                <a class="nav-link text-white">Descripcion</a>
                            </div>
                        </nav>
                </div>
            
        </thead>
        <tbody class="table bg-info" style="border:solid 3px yellow" >
            @foreach ($prospects as $prospect )
                <tr>
                    <th scope="row">
                        <td>
                            {{$prospect->name}}
                          </td>
                          <td>
                              {{$prospect->email}}
                            </td>
                            <td>
                              {{$prospect->typeServices}}
                            </td>
                            <td>
                              {{$prospect->Description}}
                            </td>
                            <td>
                                <a href="{{route('prospect.edit', $prospect)}}"><i class="fas fa-edit"></i></a>
                            </td>
                            <td> 
                            <a href="{{route('prospect.destroy', $prospect)}}" class="text-danger"><i class="fas fa-trash-alt"></i></a>
                            </td>
                    </th>
                </tr>
            @endforeach
        </tbody>
    </table>

    {{$prospects->links()}}

 </div>
 
@endsection
