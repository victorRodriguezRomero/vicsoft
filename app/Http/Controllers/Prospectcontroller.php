<?php

namespace App\Http\Controllers;

use App\Models\prospect;
use Exception;
use Illuminate\Http\Request;
use App\Http\Requests\Stormprospect;
use App\Mail\ContactenosMailable;
use Illuminate\Support\Facades\Mail;


class Prospectcontroller extends Controller
{
   public function show()
   {
      $prospects = prospect::paginate();
      return view('Prospect/index', compact('prospects'));
   }
   public function create()
   {
      return view('Prospect/create');
   }
   public function store(Stormprospect $request)
   {
   
         $correo= new ContactenosMailable($request->all());
      Mail::to('victormanrro@hotmail.ccom')->send($correo);
    $mesage='correo enviado';
         $prospects=prospect::create($request->all());
       
         return redirect()->route('prospect.create', $mesage);
   }
   public function edit($id)
   {
      $prospects = prospect::find($id);
      return view('
      Prospect/edit', compact('prospects'));
   }

   public function update(Request $request, prospect $prospects){
     
       $prospects ->update($request->all());
      return redirect()->route('prospect.index', $prospects);
   }
  public function destroy(prospect $prospect){

  $prospect->delete();
  return redirect()->route('prospect.index');
   
  }
}
