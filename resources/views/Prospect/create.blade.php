@extends('loyout.principal')
@section('title', 'prospect')
@section('content')
<div class="container pt-3">
    <h3 class="text-center">Contactenos ahora</h3>
     <div class="form row">
       <form action="{{route('prospect.store')}}" method="POST" >
        @csrf
           <div class="form-group">
               <label for="name">Nombre</label>
               <input type="text" class="form-control" name="name">
               <br>
                @error('name')
               <small> {{$message}}</small>
                    
                @enderror
               <br>
           </div>
           <div class="form-group mt-4">
            <label for="name">Correo Electronico</label>
            <input type="email" class="form-control" name="email">
            <br>
                @error('email')
               <small> {{$message}}</small>
                    
                @enderror
               <br>
        </div>
        <div class="form-group mt-4">
            <label for="name">Tipo de servicio</label>
            <select name="TypeServices" id="">
                <option value="App-Movil">App-Movil</option>
                <option value="Sistema-Web">Sistema-Web</option>
                <option value="Sistema-escritorio">App-escritorio</option>
            </select>
        </div>
        <div class="form-group mt-4">
            <label for="Description">Descripcion</label>
             <textarea name="Description" cols="10" rows="10" class="form-control" ></textarea>
             <br>
                @error('Description')
               <small> {{$message}}</small>
                    
                @enderror
               <br>
        </div>
        <div class="form-group mt-4">
            <input type="submit" class="btn btn-success" value="Contactenos">
        </div>
       </form>
     </div>

 </div>
 
@endsection
