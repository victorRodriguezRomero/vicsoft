<?php

namespace Database\Factories;

use App\Models\prospect;
use Illuminate\Database\Eloquent\Factories\Factory;

class prospectFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = prospect::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
